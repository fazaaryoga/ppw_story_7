from django.urls import path
from . import views

app_name = 'story_7'

urlpatterns = [
    path('', views.index, name='index'),
    path('confirmation/', views.confirm, name='confirmation')
]   