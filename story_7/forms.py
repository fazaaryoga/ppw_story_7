from django import forms

class StatusInputForm(forms.Form):
    name = forms.CharField(max_length= None, label = 'Name', initial='', widget = forms.TextInput({'id':'name'}))
    status_message = forms.CharField(label = 'Status', initial='', widget = forms.TextInput({'id':'status'}))
    