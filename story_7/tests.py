from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest
import time
from .models import *
from .views import *
from .forms import *

# Create your tests here.

class story7UnitTest(TestCase):

    def testMainPageExist(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def testMainPageUsesIndexHTML(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

    def testMainPageUsesIndexFunc(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def testGetConfirmPageExist(self):
        response = self.client.get('/confirmation/')
        self.assertEqual(response.status_code, 302)

    def testConfirmPageUsesConfirmHTML(self):
        response = self.client.get('/confirmation/')
        self.assertTemplateUsed('confirm.html')

    def testConfirmPageUsesConfirmFunc(self):
        found = resolve('/confirmation/')
        self.assertEqual(found.func, confirm)

    def testIndexPageCanPost(self):
        response = self.client.post('/', {'name':'test_name','status_message':'test_message'})
        count = Status.objects.all().count()
        self.assertEqual(1, count)

    def testIndexPageCanPostInvalid(self):
        response = self.client.post('/', {'name':'test_name','status_message':''})
        count = Status.objects.all().count()
        self.assertEqual(0, count)

    def testConfirmPageCanPost(self):
        response = self.client.post('/confirmation/', {'name':'test_name','status_message':'test_message'})
        self.assertContains(response, 'test_message', 1, 200, 'message is not found', False)

    def testConfirmPageCanPostInvalid(self):
        response = self.client.post('/confirmation/', {'name':'test_name','status_message':''})
        self.assertRedirects(response, '/')

class story7FunctionalTest(unittest.TestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')


    def tearDown(self):
        browser = self.browser
        browser.quit()

    def testIndexPageHasTitle(self):
        browser = self.browser
        browser.get('localhost:8000')
        ##print(browser.page_source)

        self.assertIn('', self.browser.title)

    def testConfirmPageHasTitle(self):
        browser = self.browser
        browser.get('localhost:8000/confirmation/')
        
        self.assertIn('', self.browser.title)

    def testIndexPostForm(self):
        browser = self.browser
        browser.get('localhost:8000')
        time.sleep(3)

        name = browser.find_element_by_name('name')
        message = browser.find_element_by_name('status_message')
        submit = browser.find_element_by_id('submit')

        name.send_keys('test_name')
        message.send_keys('test_message')
        submit.send_keys(Keys.RETURN)

    def testIndexPostInvalidForm(self):
        browser = self.browser
        browser.get('localhost:8000')
        time.sleep(3)

        message = browser.find_element_by_name('status_message')
        name = browser.find_element_by_name('name')
        submit = browser.find_element_by_id('submit')

        name.send_keys('test_name')
        message.send_keys('')
        submit.send_keys(Keys.RETURN)

    """def testConfirmPostForm(self):
        browser = self.browser
        browser.get('localhost:8000/confirmation/')

        name = browser.find_element_by_name('name')
        message = browser.find_element_by_name('status_message')
        submit = browser.find_element_by_id('submit')

        name.send_keys('test_name')
        message.send_keys('test_message')
        submit.send_keys(Keys.RETURN)

        yes = browser.find_element_by_id('yes')
        html = browser.page_source
        self.assertIn('test_message', html)"""

        
    """def testConfirmPostInvalidForm(self):
        browser = self.browser
        browser.get('localhost:8000/confirmation/')

        name = browser.find_element_by_id('name')
        message = browser.find_element_by_id('status')
        submit = browser.find_element_by_id('no')

        name.send_keys('test_name')
        message.send_keys('')
        submit.send_keys(Keys.RETURN)"""
    








    
        